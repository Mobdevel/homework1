fun main() {
    println(first(intArrayOf(2, 5, 3, 8, 10)))
    println(second("abba"))
}

fun first(arr: IntArray): Int {
    var sum = 0
    var count = 0
    for(i in 0..arr.size) {
        if(i % 2 == 0) {
            count++
            sum += arr[i]
        }
    }
    return sum / count
}

fun second(p: String): Boolean {
    var reversed = ""
    for(i in p.length-1 downTo 0){
        reversed += p[i]
    }
    return reversed  == p
}